if &term =~ '256color'
    " Disable Background Color Erase (BCE) so that color schemes
    " work properly when Vim is used inside tmux and GNU screen.
   set t_ut=
endif

" Auto indentation
set autoindent

" Enable line numbers
set number

" Spaces not tabs, indent settings
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent
set smartindent

" Highlight all search matches
set hlsearch
set ignorecase

" Highlight matching bracket
set showmatch
set matchtime=0

" Syntax highlight
syntax on

" Plugins (vim-plug)
call plug#begin('~/.vim/plugged')
    " Color schemes
    Plug 'erichdongubler/vim-sublime-monokai'
    " Autocomplete
    Plug 'ycm-core/YouCompleteMe'
    " Numbertoggle
    Plug 'jeffkreeftmeijer/vim-numbertoggle'
    " Status bar
    Plug 'itchyny/lightline.vim'
call plug#end()

colorscheme sublimemonokai

" Status bar
set laststatus=2
let g:lightline = {
\ 'colorscheme': 'wombat'
\ }

" Remove vim background to use terminal background
" hi Normal ctermbg=NONE

" Remove line Number column color
highlight LineNr ctermbg=NONE

" Highlight current line
set cursorline
highlight CursorLine ctermbg=Black

" YCM settings
nnoremap <C-]> :YcmCompleter GoToDeclaration<CR>
nnoremap <C-\> :YcmCompleter GoToDefinition<CR>

" Numbertoggle settings
augroup numbertoggle
    autocmd!
    autocmd InsertLeave * set relativenumber
    autocmd InsertEnter * set norelativenumber

" Clear search highlight on enter
nnoremap <CR> :noh<CR><CR>

" Autofill braces
inoremap {<CR> {<CR>}<C-o>O

