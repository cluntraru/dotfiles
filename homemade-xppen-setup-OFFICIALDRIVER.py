#!/bin/python3

import subprocess
from subprocess import Popen, PIPE
import time

xinput_pen_name = "XPPEN Tablet Pen (0)"

def xinput_contains_pen():
    cmd = "xinput | grep \"" + xinput_pen_name + "\""
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    xinput_out, xinput_err = p.communicate()
    return len(xinput_out) > 0

cmd = "sudo sh -c \"/home/cluntraru/.xp-pen/Linux_Pentablet_V1.2.13.1/Pentablet_Driver.sh\""
p = Popen(cmd, shell = True)

iterations = 0
while (not xinput_contains_pen()):
    time.sleep(1)
    iterations += 1
    if (iterations >= 10):
        print("Failed to detect pen. Please try again.")
        exit(1)
cmd = "xinput --map-to-output \"" + xinput_pen_name + "\" HDMI-A-0"
p = Popen(cmd, shell = True)
exit(0);

